package com.cisco.dcloud.cxdemo.cvp;

import java.io.InputStream;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.json.JSONArray;
import org.json.JSONObject;

import com.cisco.dcloud.cxdemo.cvp.annotation.RestPath;
import com.cisco.dcloud.cxdemo.cvp.bean.BaseApiBean;
import com.cisco.dcloud.cxdemo.cvp.bean.Server;

public class CvpRestClient extends WebClient{

	protected WebTarget target;
	protected Client client;
	protected String url;

	public CvpRestClient(String restServer, String restUsername, String restPassword, boolean restIgnoreSsl) {
		super(restIgnoreSsl);
		this.url = "https://"+restServer+":8111/cvp-config/";
		client = getClient();
		// add BASIC authentication
		HttpAuthenticationFeature basicAuth = HttpAuthenticationFeature.basic(
				restUsername,
				restPassword
				);
		client.register(basicAuth);
		target = client.target(url);
	}

	public <T extends BaseApiBean> Response list(Class<T> beanType) throws CvpRestException {
		return list(beanType, new JSONObject());
	}

	public <T extends BaseApiBean> Response list(Class<T> beanType, JSONObject params) throws CvpRestException {
		String path = beanType.getAnnotation(RestPath.class).value();
		// add all query params from input parameter
		JSONArray names = params.names();
		for (int i = 0; i < names.length(); i++) {
			String key = names.getString(i);
			Object value = params.get(key);
			target = target.queryParam(key, value);
		}

		// replace URL parameters with values
		Response response = target.path(path)
				.request(MediaType.APPLICATION_JSON_TYPE)
				.get( Response.class );
		return response;
	}

	public <T extends BaseApiBean> Response post(Class<T> beanType, JSONArray serverList) {
		String path = beanType.getAnnotation(RestPath.class).value();

		JSONObject servers = new JSONObject().put("server",serverList);
		JSONObject synchronizeWrapper = new JSONObject().put("servers", servers);
		JSONObject wrapper = new JSONObject().put("synchronize", synchronizeWrapper);

		return target.path(path)
				.request(MediaType.APPLICATION_JSON_TYPE)
				.post( Entity.entity( wrapper.toString(), MediaType.APPLICATION_JSON_TYPE ), Response.class );
	}

	public Response getContent(String refUrl) {
		// use absolute refUrl
		Response response = client.target(refUrl)
				.request(MediaType.APPLICATION_JSON_TYPE)
//				.accept(MediaType.MULTIPART_FORM_DATA)
				.get(Response.class)
				;
		return response;
	}

	public Response get(String path) {
		Response response = target
				.path(path)
				.request(MediaType.APPLICATION_JSON_TYPE)
				.get(Response.class)
				;
		return response;
	}

	public <T extends BaseApiBean> Response create(Class<T> beanType, InputStream stream, String path, String filename) {
		String urlPath = beanType.getAnnotation(RestPath.class).value();
		FormDataMultiPart part = makeFormPart(stream, path, filename);
		Response response = target.path(urlPath)
				.request( MediaType.APPLICATION_JSON_TYPE )
				.post( Entity.entity( part, MediaType.MULTIPART_FORM_DATA ), Response.class );
		return response;
	}

	public <T extends BaseApiBean> Response update(Class<T> beanType, InputStream stream, String path, String filename) {
		String urlPath = beanType.getAnnotation(RestPath.class).value();
		FormDataMultiPart part = makeFormPart(stream, path, filename);
		Response response = target.path(urlPath)
				.request( MediaType.APPLICATION_JSON_TYPE )
				.put( Entity.entity( part, MediaType.MULTIPART_FORM_DATA ), Response.class );
		return response;
	}
	
	private FormDataMultiPart makeFormPart (InputStream stream, String path, String filename) {
		FormDataMultiPart part = new FormDataMultiPart();
		part.field( "mediafile", stream, MediaType.TEXT_PLAIN_TYPE );
		JSONObject mediaUploadConfig = new JSONObject();
		JSONObject config = new JSONObject();
		config.put( "path", path );
		config.put( "filename", filename );
		mediaUploadConfig.put( "mediafile", config );
		part.field( "mediaUploadConfig" , mediaUploadConfig.toString() );
		return part;
	}
}
