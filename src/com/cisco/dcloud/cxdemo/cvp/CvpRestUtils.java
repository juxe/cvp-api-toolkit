package com.cisco.dcloud.cxdemo.cvp;

import java.io.InputStream;

import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.cisco.dcloud.cxdemo.cvp.annotation.RestPath;
import com.cisco.dcloud.cxdemo.cvp.bean.Mediafile;
import com.cisco.dcloud.cxdemo.cvp.bean.Server;
import com.cisco.dcloud.cxdemo.cvp.bean.Synchronize;

public class CvpRestUtils {
	public static void synchronizeVxmlServers(CvpRestClient restClient) throws CvpRestException {
		synchronizeServers(restClient, getVxmlServers(restClient));
	}

	public static void synchronizeMediaServers(CvpRestClient restClient) throws CvpRestException {
		synchronizeServers(restClient, getMediaServers(restClient));
	}

	public static void synchronizeServers(CvpRestClient restClient, JSONArray serverList) {
		restClient.post(Synchronize.class, serverList);
	}

	public static JSONArray getServers(CvpRestClient restClient) throws CvpRestException {
		return processResponse(restClient.list(Server.class));
	}

	private static JSONArray processResponse(Response response) throws CvpRestException {
		String readEntity = response.readEntity(String.class);
		if (response.getStatus() >= 200 && response.getStatus() < 300) {
			JSONObject json = new JSONObject(readEntity);
			JSONObject results = json.getJSONObject("results");
			JSONObject pageInfo = results.getJSONObject("pageInfo");
			return getArray(results, Mediafile.class.getAnnotation(RestPath.class).value());
		} else {
			//			{"apiErrors":
			//			{"apiError":
			//			{"errorDetail":
			//			{"@type":"invalidValueErrorDetail",
			//			"name":"summary",
			//			"actual":"false",
			//			"valid":"true"
			//				}
			//		}}}
			throw new CvpRestException("Error: CVP returned status " + response.getStatus() + " - " + readEntity);
		}
	}

	public static JSONArray getVxmlServers(CvpRestClient restClient) throws CvpRestException {
		return filterVxmlServers(getServers(restClient));
	}

	public static JSONArray getMediaServers(CvpRestClient restClient) throws CvpRestException {
		return filterMediaServers(getServers(restClient));
	}

	private static JSONArray filterVxmlServers(JSONArray servers) {
		JSONArray ret = new JSONArray();
		for(int i=0; i<servers.length(); i++) {
			JSONObject o = servers.getJSONObject(i);
			if(o.getString("type").equals("VXML")) {
				ret.put(o);
			}
		}
		return ret;
	}

	public static JSONArray filterMediaServers(JSONArray servers) {
		JSONArray ret = new JSONArray();
		for(int i=0; i<servers.length(); i++) {
			JSONObject o = servers.getJSONObject(i);
			if(o.getString("type").equals("MEDIA")) {
				ret.put(o);
			}
		}
		return ret;
	}

	public static JSONArray getArray(JSONObject responseJson, String beanName) {
		Object wrapper = responseJson.get(beanName + "s");
		if(wrapper instanceof JSONObject) { // are there any entries configured?
			Object o = ((JSONObject) wrapper).opt(beanName);
			if(o == null) { // no entries
				return new JSONArray();
			} else {
				if(o instanceof JSONArray) { //more than one entry
					return (JSONArray) o;
				} else if (o instanceof JSONObject) { // one entry
					JSONArray ret = new JSONArray();
					ret.put(o);
					return ret;
				} else { // unexpected response?
					return new JSONArray();
				}
			}
		} else { // instanceof String - no entries configured in system
			return new JSONArray();
		}
	}

	public static JSONArray listMediaFiles(CvpRestClient cvp) throws CvpRestException {
		return listMediaFiles(cvp, null);
	}


	/**
	 * Returns a list of CVP Mediafiles from CVP OAMP APIs
	 * valid search queries:
	 *  "filename:1* AND path:en-us/sys"
	 *  "filename:hello*.wav"
	 *  "filename:0.wav"
	 *  "path:es-mx/sys"
	 * @param cvp rest client
	 * @param q the search query
	 * @return
	 * @throws CvpRestException
	 */
	public static JSONArray listMediaFiles(CvpRestClient cvp, String q) throws CvpRestException {
		return listMediaFiles(cvp, q, false, 0, 50);
	}

	/**
	 * Returns a list of CVP Mediafiles from CVP OAMP APIs
	 * valid search queries:
	 *  "filename:1* AND path:en-us/sys"
	 *  "filename:hello*.wav"
	 *  "filename:0.wav"
	 *  "path:es-mx/sys"
	 * @param cvp rest client
	 * @param q the search query
	 * @param summary whether to return summarized audio file objects
	 * @return
	 * @throws CvpRestException
	 */
	public static JSONArray listMediaFiles(CvpRestClient cvp, String q, boolean summary) throws CvpRestException {
		return listMediaFiles(cvp, q, summary, 0, 50);
	}

	/**
	 * Returns a list of CVP Mediafiles from CVP OAMP APIs
	 * valid search queries:
	 *  "filename:1* AND path:en-us/sys"
	 *  "filename:hello*.wav"
	 *  "filename:0.wav"
	 *  "path:es-mx/sys"
	 * @param cvp rest client
	 * @param q the search query
	 * @param summary whether to return summarized audio file objects
	 * @param startIndex the starting index of paginated results to return
	 * @param resultsPerPage the number of paginated results to return. maximum value is 50.
	 * @return
	 * @throws CvpRestException
	 */
	public static JSONArray listMediaFiles(CvpRestClient cvp, String q, boolean summary, int startIndex, int resultsPerPage) throws CvpRestException {
		JSONObject params = new JSONObject();
		params.put("startIndex", startIndex);
		params.put("resultsPerPage", resultsPerPage);
		if (summary) {
			params.put("summary", "true");
		}
		params.put("q", q);
		Response response = cvp.list(Mediafile.class, params);
		String readEntity = response.readEntity(String.class);
		if (response.getStatus() >= 200 && response.getStatus() < 300) {
			JSONObject json = new JSONObject(readEntity);
			JSONObject results = json.getJSONObject("results");
			JSONObject pageInfo = results.getJSONObject("pageInfo");
			int totalResults = pageInfo.getInt("totalResults");
			// recurse and append results
			JSONArray array = getArray(results, Mediafile.class.getAnnotation(RestPath.class).value());
			// are there more results to get?
			if (startIndex + resultsPerPage < totalResults) {
				JSONArray nextResults = listMediaFiles(cvp, q, summary, startIndex + resultsPerPage, 50);
				// append nextResults onto results
				for (int i = 0; i < nextResults.length(); i++) {
					array.put(nextResults.get(i));
				}
			}
			return array;
		} else {
			//{
			//    "apiErrors": {
			//        "apiError": {
			//            "errorDetail": {
			//                "@type": "invalidValueErrorDetail",
			//                "name": "summary",
			//                "actual": "false",
			//                "valid": "true"
			//            }
			//        }
			//    }
			//}
			try {
				JSONObject errorDetail = new JSONObject(readEntity).getJSONObject("apiErrors").getJSONObject("apiError").getJSONObject("errorDetail");
				throw new CvpRestException("CVP returned status " + response.getStatus() + ": " + errorDetail.toString(2));
			} catch (JSONException e) {
				throw new CvpRestException("CVP returned status " + response.getStatus() + ": " + readEntity);
			}
		}
	}

	public static InputStream getContent(CvpRestClient restClient, String refUrl) {
		Response contents = restClient.getContent(refUrl + "/content");
		return contents.readEntity(InputStream.class);
	}

	public static JSONObject getMediaFile(CvpRestClient restClient, String path) {
		Response contents = restClient.get(path);
		String readEntity = contents.readEntity(String.class);
		JSONObject jsonObject = new JSONObject(readEntity);
		return jsonObject;
	}

	public static JSONObject deployAudioFile(CvpRestClient restClient, InputStream is, String path, String filename) throws CvpRestException {
		Response response = restClient.create(Mediafile.class, is, path, filename);
		String readEntity = response.readEntity(String.class);
		switch(response.getStatus()) {
		case 200: // mediafile UPDATE returns malformed response
		case 202:
			try {
				return new JSONObject(readEntity);
			} catch (JSONException e) {
				return new JSONObject();
			}
		default:
			try {
				return new JSONObject(readEntity);
			} catch (JSONException e) {
				return new JSONObject().put("results", readEntity);
			}
		}
	}

	public static void updateAudioFile(CvpRestClient restClient, InputStream is, String path, String filename) throws CvpRestException {
		Response response = restClient.update(Mediafile.class, is, path, filename);
		String readEntity = response.readEntity(String.class);
		switch(response.getStatus()) {
		case 200: // mediafile UPDATE returns malformed response
		case 202:
			return;
		default:
			try {
				throw new CvpRestException(new JSONObject(readEntity).toString(2));
			} catch (JSONException e) {
				throw new CvpRestException(readEntity);
			}
		}
	}
}
