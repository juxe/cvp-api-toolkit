package com.cisco.dcloud.cxdemo.cvp;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;

import org.glassfish.jersey.client.ClientProperties;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.glassfish.jersey.filter.LoggingFilter;
import org.glassfish.jersey.media.multipart.MultiPartFeature;

public class WebClient {
	protected static final String LOCATION = "Location";
	protected boolean ignoreSSL;
	
	public WebClient() {
		this.ignoreSSL = false;
	}
	
	public WebClient(boolean ignoreSSL) {
		this.ignoreSSL = ignoreSSL;
	}

	protected Client getClient(String username, String password) {
		Client client = getClient();
		// add BASIC authentication
		HttpAuthenticationFeature basicAuth = HttpAuthenticationFeature.basic(username,password);
		client.register( basicAuth );
		return client;
	}
	
	protected Client getClient() {
		ClientBuilder builder = null;
		if(ignoreSSL) {
			try {
				// accept all SSL certificates
				builder = IgnoreSSLClientBuilder();
			} catch (KeyManagementException | NoSuchAlgorithmException e) {
				throw new RuntimeException("Error creating REST client that ignores SSL certificates", e);
			}
		} else {
			// standard client - use normal SSL certificate checking
			builder = SSLClientBuilder();
		}
		// don't follow redirects
		builder = builder.property(ClientProperties.FOLLOW_REDIRECTS, Boolean.FALSE);
		// add support for multipart form data, for uploading files using HTML forms
		builder = builder.register(MultiPartFeature.class);
		if(isDebugEnvironment()) {
			builder = builder.register(new LoggingFilter());
		}
		return builder.build();
	}

	public static boolean isDebugEnvironment() {
		return java.lang.management.ManagementFactory.getRuntimeMXBean().getInputArguments().toString().indexOf("-agentlib:jdwp") > 0;
	}

	private static ClientBuilder SSLClientBuilder() {
		return ClientBuilder.newBuilder();
	}

	private static ClientBuilder IgnoreSSLClientBuilder() throws KeyManagementException, NoSuchAlgorithmException {
		SSLContext sslcontext = SSLContext.getInstance("TLS");
		sslcontext.init(null, new TrustManager[]{
				new X509TrustManager() {
					public void checkClientTrusted(X509Certificate[] arg0, String arg1) {}
					public void checkServerTrusted(X509Certificate[] arg0, String arg1) {}
					public X509Certificate[] getAcceptedIssuers() { return new X509Certificate[0]; }
				}
		}, new java.security.SecureRandom());
		return ClientBuilder.newBuilder().sslContext(sslcontext).hostnameVerifier((s1, s2) -> true);
	}
}
