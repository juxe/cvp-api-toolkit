package com.cisco.dcloud.cxdemo.cvp;

@SuppressWarnings("serial")
public class InvalidInputException extends Exception {

	public InvalidInputException(String m) {
		super(m);
	}

}
