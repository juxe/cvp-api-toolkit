package com.cisco.dcloud.cxdemo.cvp.bean;

import com.cisco.dcloud.cxdemo.cvp.annotation.RestPath;
@RestPath("mediafile")
public class Mediafile extends BaseApiBean {

//	private static final String BASE_PATH = "/mediafile";
//	private static final String GET_MEDIAFILE = BASE_PATH + "/{path}/{filename}";
//	private static final String GET_MEDIAFILE_CONTENT = GET_MEDIAFILE + "/content";

//	public Mediafile(String restServer, String restUsername, String restPassword, boolean restIgnoreSsl) {
//		super(restServer, restUsername, restPassword, restIgnoreSsl);
//	}

//	public JSONObject get( String path, String filename ) {
//		// replace URL parameters with values
//		Response response = target.path(GET_MEDIAFILE)
//				.resolveTemplate("path", path)
//				.resolveTemplate("filename", filename)
//				.request(MediaType.APPLICATION_JSON_TYPE)
//				.get(Response.class);
//		return new JSONObject(response.readEntity(String.class));
//	}

//	public JSONObject delete( String path, String filename ) {
//		// replace URL parameters with values
//		Response response = target.path(GET_MEDIAFILE)
//				.resolveTemplate("path", path)
//				.resolveTemplate("filename", filename)
//				.request(MediaType.APPLICATION_JSON_TYPE)
//				.delete(Response.class);
//		return new JSONObject(response.readEntity(String.class));
//	}
//
//	public JSONObject getContent( String path, String filename ) {
//		// replace URL parameters with values
//		Response response = target.path(GET_MEDIAFILE_CONTENT)
//				.resolveTemplate("path", path)
//				.resolveTemplate("filename", filename)
//				.request(MediaType.APPLICATION_JSON_TYPE)
//				.get(Response.class);
//		return new JSONObject(response.readEntity(String.class));
//	}

	/**
	 * @param params
	 * 
	 * From the API documentation, https://developer.cisco.com/fileMedia/download/7865fc47-db28-469c-b87c-c0e71b67d960
{
   "filename": {
      "Description": "Text parameter with first few letters of file",
      "Required": false,
      "Validations": "Should not contain any starting wildcard characters"
   },
   "path": {
      "Description": "Text parameter with the path name.",
      "Required": false,
      "Values": "Should be one of the internal path names.",
      "Validations": "No wild characters allowed."
   },
   "summary": {
      "Description": "Text parameter to indicate whether only file name and path is listed or full details to be listed.",
      "Required": false,
      "Values": "true",
      "Validations": "Only allowed values.",
      "Notes": "URL query parameter. Default is false."
   },
   "startIndex": {
      "Description": "Starting index of the result list.",
      "Required": false,
      "Values": "Zero-based - 0 is the first element. DEFAULT = 0.",
      "Validations": "zero or more.",
      "Notes": "URL query parameter."
   },
   "resultsPerPage": {
      "Description": "Specifies number of elements to retrieve.",
      "Required": false,
      "Values": "MIN=1. DEFAULT=25 MAX=50",
      "Validations": "One or more.",
      "Notes": "URL query parameter."
   }
}

	 * @return Response object
	 */
//	public Response list( JSONObject params ) {
//		// add all 	params as URL query parameters
//		Iterator<?> keys = params.keys();
//		while( keys.hasNext() ) {
//			String key = (String)keys.next();
//			Object value = params.get(key);
//			target = target.queryParam(key, value);
//		}
//
//		// replace URL parameters with values
//		Response response = target.path(BASE_PATH)
//				.request(MediaType.APPLICATION_JSON_TYPE)
//				.get(Response.class);
//		return response;
//	}
	
	/*
	 * params: path, filename, servers, server, file
	 * 
	 */
	
	/*
The parameters should be passed as multipart/form-data contents, with the
text parametersin a JSON/XML part followed by the binary data as a separate
part.
Name: path
Description: Text parameter containing the relative path in the media location.
Required: Yes.
Values: Relative path names. For example: en-us/app as used by the
application.
Validations: Should not contain any (.) or should not be an absolute path.
Notes: Passed in XML/JSON body.
Name: filename
Description: Text parameter containing the media file name.
Required: Yes.
Values: String values with less than 100 characters.
Validations: Should not contain special characters like "~", >, <.
Notes: Passed in XML/JSON body.
Name: servers
Description: List of server reference URLs.
Required: No.
Values: Contains multiple <server> elements.
Notes: Optional parameter containing the server ref list, passed in XML/JSON
body.
Name: server
Description: Reference of individual Unified CVP media server.
Required: No.
Values: As depicted in CVP Servers section.
Notes: Optional parameter containing the server ref, passed in XML/JSON
body.
Name: file
Description: This file will be uploaded.
Required: Yes.
Values: Multipart form posts as handled by the FORM POST method.
Validations: Size of individual file should not exceed MAX_FILE_SIZE
(3MB).
Notes: Need to be appended by either browser FORM POST execution or
equivalent code, if done programmatically.
	 */
	
//	public JSONObject create( InputStream stream, String path, String filename ) {
//		FormDataMultiPart part = makeFormPart(stream, path, filename);
//		Response response = target.path( BASE_PATH )
//				.request( MediaType.APPLICATION_JSON_TYPE )
//				.post( Entity.entity( part, MediaType.MULTIPART_FORM_DATA ), Response.class );
//		return new JSONObject(response.readEntity(String.class));
//	}
//	
//	public Response update( InputStream stream, String path, String filename ) {
//		FormDataMultiPart part = makeFormPart(stream, path, filename);
//		Response response = target.path( BASE_PATH )
//				.request( MediaType.APPLICATION_JSON_TYPE )
//				.put( Entity.entity( part, MediaType.MULTIPART_FORM_DATA ), Response.class );
//		return response;
//	}
	
//	private FormDataMultiPart makeFormPart (InputStream stream, String path, String filename) {
//		FormDataMultiPart part = new FormDataMultiPart();
//		part.field( "mediafile", stream, MediaType.TEXT_PLAIN_TYPE );
//		JSONObject mediaUploadConfig = new JSONObject();
//		JSONObject config = new JSONObject();
//		config.put( "path", path );
//		config.put( "filename", filename );
//		mediaUploadConfig.put( "mediafile", config );
//		part.field( "mediaUploadConfig" , mediaUploadConfig.toString() );
//		return part;
//	}
}
