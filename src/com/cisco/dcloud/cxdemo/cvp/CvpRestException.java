package com.cisco.dcloud.cxdemo.cvp;

@SuppressWarnings("serial")
public class CvpRestException extends Exception {

	public CvpRestException(String m) {
		super(m);
	}

	public CvpRestException(Exception e) {
		super(e);
	}

}
